function inputMask() {
    if ($('.js-phone-mask').length) {
        $('.js-phone-mask').mask('+7(999) 999 - 99 - 99');
    }
}
$(document).ready(function(){
	if (document.body.clientWidth <= '800') {
		$('#mobile_filter').after( $('.catalog_column_left .d-filter__mob') );
	}
	//$(".d-filter__new-hdr").hover(function(){ 		
	//	 $(this).siblings($('.d-filter__mob-icon')).css({'fill' : '#ff733f'});		 
	//},function(){		
	//	 $(this).siblings($('.d-filter__mob-icon')).css({'fill' : '#000'});		
	//});
});
$(document).ready(function(){
	if (document.body.clientWidth <= '1024') {
		var im = $('li.navigation-list__item');
		var element_im = $('.menu-small-list');
		$('.mobile_menu').append( $('.header-bottom .js-main-menu .js-main-menu-list') );
		element_im.prepend(im);
		im.toggleClass('navigation-list__item menu-small-list-item');
		im.children('div').toggleClass('navigation-item menu-small-item');
		im.find('a').toggleClass('menu-small-item__link').removeClass('navigation-item__link navigation-item__link--active');
	}
	//$(".d-filter__new-hdr").hover(function(){ 		
	//	 $(this).siblings($('.d-filter__mob-icon')).css({'fill' : '#ff733f'});		 
	//},function(){		
	//	 $(this).siblings($('.d-filter__mob-icon')).css({'fill' : '#000'});		
	//});
});
$(document).ready(function(){
	if ($('.mebel_all_line').length) {
	$('.mebel_all_line').isotope({
	  itemSelector: '.grid-item',
	  getSortData: {
    	name: '.alpha_block',
	  },
	  sortBy: 'name',
	  masonry: {
		columnWidth: '.grid-item'
	  }
	});
	}
});
$(document).ready(function(){
	if (document.body.clientWidth >= '1024') {
		$(".all_mebel").hover(function() { 
			$('.activate_grid').isotope({
			  itemSelector: '.activate_grid>.dropbox',
			  masonry: {
				columnWidth: 1
			  }
			});
		});
	}
});
$(document).ready(function(){
	if ($('.owl-review').length) {
	var owl_review = $(".owl-review"); //блок с каруселью

	owl_review.owlCarousel({ //инициализируем карусь
		loop:true,
		margin:20,
		nav:false,
		responsive:{
			0:{
				items:2,
				margin:10,
			},
			600:{
				items:2
			},
			1025:{
				items:4
			}
		}
	});

	//добавляем к кнопкам в блоке с каруселью обработчики
	$(".next-review").click(function() { 
		owl_review.trigger('next.owl.carousel');
	});

	$(".prev-review").click(function() {
		owl_review.trigger('prev.owl.carousel');
	});
	}
});
$(document).ready(function(){
	if ($('.owl-review_2').length) {
	var owl_review_2 = $(".owl-review_2"); //блок с каруселью

	owl_review_2.owlCarousel({ //инициализируем карусь
		loop:true,
		margin:20,
		nav:false,
		responsive:{
			0:{
				items:2,
				margin:10,
			},
			600:{
				items:2
			},
			1025:{
				items:4
			}
		}
	});

	//добавляем к кнопкам в блоке с каруселью обработчики
	$(".next-review-2").click(function() { 
		owl_review_2.trigger('next.owl.carousel');
	});

	$(".prev-review-2").click(function() {
		owl_review_2.trigger('prev.owl.carousel');
	});
	}
});
$(document).ready(function(){
	if ($('.slider_insta').length) {
	var slider_insta = $(".slider_insta"); //блок с каруселью

	slider_insta.owlCarousel({ //инициализируем карусь
		loop:true,
		//margin:20,
		nav:false,
		merge:true,
		responsive:{
			0:{
				items:2
			},
			600:{
				items:3
			},
			1000:{
				items:5
			}
		}
	});

	//добавляем к кнопкам в блоке с каруселью обработчики
	$(".next-insta").click(function() { 
		slider_insta.trigger('next.owl.carousel');
	});

	$(".prev-insta").click(function() {
		slider_insta.trigger('prev.owl.carousel');
	});
	}
});
/*spinner*/
function spinner() {
    if ($('.js-spinner').length) {
        $('.js-spinner-plus').on('click', function () {
            var valueInput = $(this).siblings('input').val(),
                plus;
            if (valueInput >= 0) {
                plus = parseInt(valueInput) + 1;
                $(this).siblings('input').val(plus);
            }
            if (valueInput == '') {
                $(this).siblings('input').val(1);
            }
        })
        $('.js-spinner-minus').on('click', function () {
            var valueInput = $(this).siblings('input').val(),
                minus;
            if (valueInput > 0) {
                minus = parseInt(valueInput) - 1;
                $(this).siblings('input').val(minus);
            }
            if (valueInput == '') {
                $(this).siblings('input').val(0);
            }
        })
        $('.js-spinner-input').bind("change keyup input click", function (event) {
            var valueInput = $(this).val(),
                plus, minus;

            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9]/g, '');
            }
            if (event.keyCode == 38) {
                if (valueInput >= 0) {
                    plus = parseInt(valueInput) + 1;
                    $(this).val(plus);
                }
            }
            if (event.keyCode == 40) {
                if (valueInput > 0) {
                    minus = parseInt(valueInput) - 1;
                    $(this).val(minus);
                }
            }
        });
    }
}
/*spinner end*/

function catalogDrop() {
    if ($('.js-main-menu-trigger').length) {
        var open = false;
        $('.js-main-menu-trigger').on('click', function () {
			$('body').toggleClass('no_overflow');
            $(this).toggleClass("active");
            $(this).closest(".js-main-menu").find(".js-main-menu-list").slideToggle();
            open = !open;
        })

        $(document).on('click', function (e) {
            if (open) {
                var isBtn = !!$(e.target).closest('.js-main-menu-trigger').length,
                    isMenu = !!$(e.target).closest('.drop-item').length;
                if (!isBtn && !isMenu) {
                    console.log(1);
                    $('.js-main-menu-trigger').removeClass('active');
                    $('.js-main-menu-trigger').closest(".js-main-menu").find(".js-main-menu-list").slideUp();
                    open = false;
                }
            }
        })

    }
}

function menuDrop() {
    if ($('.js-menu-drop-trigger').length) {
        var open = false;
        $('.js-menu-drop-trigger').on('click', function () {
            $(this).toggleClass("active");
            $(this).closest(".js-menu-drop").find(".js-menu-drop-list").slideToggle();
            open = !open;
        })

        $(document).on('click', function (e) {
            if (open) {
                var isBtn = !!$(e.target).closest('.js-menu-drop-trigger').length,
                    isMenu = !!$(e.target).closest('.js-menu-drop-list').length;
                if (!isBtn && !isMenu) {
                    console.log(1);
                    $('.js-menu-drop-trigger').removeClass('active');
                    $('.js-menu-drop-trigger').closest(".js-menu-drop").find(".js-menu-drop-list").slideUp();
                    open = false;
                }
            }
        })

    }
}

function basketDrop() {
    if ($('.js-basket-drop-trigger').length) {
        var open = false;
        $('.js-basket-drop-trigger').on('click', function () {
            $(this).toggleClass("active");
            $(this).closest(".js-basket-drop").find(".js-basket-drop-list").slideToggle();
            open = !open;
        })

        $(document).on('click', function (e) {
            if (open) {
                var isBtn = !!$(e.target).closest('.js-basket-drop-trigger').length,
                    isMenu = !!$(e.target).closest('.js-basket-drop-list').length;
                if (!isBtn && !isMenu) {
                    console.log(1);
                    $('.js-basket-drop-trigger').removeClass('active');
                    $('.js-basket-drop-trigger').closest(".js-basket-drop").find(".js-basket-drop-list").slideUp();
                    open = false;
                }
            }
        })

    }
}

function sidebarAccordeon() {
    var $title = $('.js-sidebar-title'),
        $inner = $('.js-sidebar-inner');
    if ($inner.length) {
        $title.on('click', function () {
            if (!$(this).hasClass('active')) {
                $(this).addClass('active').parent().find($inner).slideDown(200);
            } else {
                $(this).removeClass('active').parent().find($inner).slideUp(200);
            }
        });
    }
}

function accordeon() {
    var $trigger = $('.js-accordeon__trigger'),
        $container = $('.js-accordeon-list'),
        $inner = $('.js-accordion__inner'),
        $item = $('.js-accordeon-item');

    if ($container.length) {

        $container.each(function (index, el) {
            var containerInspection = $(this).data('resolution'),
                $innerContent = $(this).find($inner);

            if (containerInspection) {
                $(window).on('resize load', function () {
                    if (window.innerWidth <= 1023) {
                        $innerContent.hide();
                    } else if (window.innerWidth > 1023) {
                        $innerContent.show();
                    }
                })
            } else if (!containerInspection) {
                $innerContent.hide();
            }
        });

        $trigger.on('click', function (e) {
            var thisItem = $(this).closest($item),
                $this = $(this);
            thisContainer = $(this).closest($container),
                containerInspection = thisContainer.data('resolution');


            function open() {
                if (!$this.closest($item).hasClass('active')) {
                    thisContainer.find($item).removeClass('active').find($inner).stop().slideUp(200);
                    thisContainer.find($trigger).removeClass('active');
                    thisItem.addClass('active').find($inner).stop().slideDown(200);
                    $this.addClass('active');
                } else {
                    thisItem.removeClass('active').find($inner).stop().slideUp(200);
                    $this.removeClass('active');
                }
            }

            if (containerInspection && window.innerWidth <= 1023) {
                open();
            } else if (!containerInspection) {
                open();
            }

            e.preventDefault();
        });

    }
}

/* placeholder */
function placeholder(objInputs) {
    if (objInputs.length) objInputs.placeholder();
}
/* placeholder end */

/* autoFooter */
function autoFooter() {
    var wrapper = $('.wrapper'),
        footer = $('.footer'),
        footerHeight = footer.outerHeight();
    footer.css('margin-top', -footerHeight);
    wrapper.css('padding-bottom', footerHeight);
}
/* autoFooter end */

/* sliders */
function sliders() {
    // js-main-slider
    if ($('.js-main-slider').length) {
        $('.js-main-slider').slick({
            dots: true,
            arrows: false,
            infinite: true,
            speed: 300,
            fade: true,
            autoplay: true,
            autoplaySpeed: 4000,
            slidesToShow: 4,
            slidesToScroll: 4,
            adaptiveHeight: true,
            cssEase: 'ease-in-out',
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }
            ]
        });
    }
    $('.js-address-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.js-address-slider-nav',
        responsive: [
            {
                breakpoint: 761,
                settings: {
                    arrows: true
                }
            }
        ]
    });
    $('.js-address-slider-nav').slick({
        slidesToShow: 8,
        slidesToScroll: 1,
        asNavFor: '.js-address-slider',
        centerMode: true,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 5
                }
            }
        ]
    });


    $('.js-gallery__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.js-gallery__nav',
        responsive: [
            {
                breakpoint: 761,
                settings: {
                    arrows: true
                }
            }

        ]

    });
    $('.js-gallery__nav').slick({
        infinite: true,
        slidesToShow: 12,
        slidesToScroll: 1,
        asNavFor: '.js-gallery__slider',
        arrows: true,
        centerMode: true,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 7
                }
            },
            {
                breakpoint: 761,
                settings: {
                    arrows: false,
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 580,
                settings: {
                    arrows: false,
                    slidesToShow: 3
                }
            }

        ]
    });


    $('.js-gallery-product__main').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.js-gallery-product__nav',
        // adaptiveHeight:true,
        responsive: [
            {
                breakpoint: 770,
                settings: {
                    arrows: true
                }
            }

        ]

    });
    $('.js-gallery-product__nav').slick({
        infinite: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.js-gallery-product__main',
        arrows: true,
        variableWidth:true,
        // centerMode: true,
        focusOnSelect: true

    });

    $('.js-large-product__main').slick({
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: '<span class="slick-prev"><svg class="icon-arrow-left"><use xlink:href="#arrow-tobottom"></use></svg></span>',
        nextArrow: '<span class="slick-next"><svg class="icon-arrow-right"><use xlink:href="#arrow-tobottom"></use></svg></span>',
        fade: true,
        asNavFor: '.js-large-product__nav',
        adaptiveHeight: false

    });
    $('.js-large-product__nav').slick({
        infinite: false,
        // slidesToShow: 5,
        variableWidth: true,
        slidesToScroll: 1,
        asNavFor: '.js-large-product__main',
        arrows: true,
        focusOnSelect: true,
        prevArrow: '<span class="slick-prev"><svg class="icon-arrow-left"><use xlink:href="#arrow-tobottom"></use></svg></span>',
        nextArrow: '<span class="slick-next"><svg class="icon-arrow-right"><use xlink:href="#arrow-tobottom"></use></svg></span>',


    });

    //large gallery with modul's products
    $('.js-large-product-module__main').slick({
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: '<span class="slick-prev"><svg class="icon-arrow-left"><use xlink:href="#arrow-tobottom"></use></svg></span>',
        nextArrow: '<span class="slick-next"><svg class="icon-arrow-right"><use xlink:href="#arrow-tobottom"></use></svg></span>',
        fade: true,
        asNavFor: '.js-large-product-module__nav',
        adaptiveHeight: false

    });
    $('.js-large-product-module__nav').slick({
        infinite: false,
        variableWidth: true,
        slidesToScroll: 1,
        asNavFor: '.js-large-product-module__main',
        arrows: true,
        focusOnSelect: true,
        prevArrow: '<span class="slick-prev"><svg class="icon-arrow-left"><use xlink:href="#arrow-tobottom"></use></svg></span>',
        nextArrow: '<span class="slick-next"><svg class="icon-arrow-right"><use xlink:href="#arrow-tobottom"></use></svg></span>',


    });
}
/* sliders end */

/* accordMulti */
function accordMulti() {
    if ($('.js-accord-link').length) {
        $('.js-accord-link').on('click', function (e) {
            var cur = $(this);
            var parent = cur.parents('.js-accord-parent:eq(0)');
            var main_parent = parent.parents('.js-accord-main-parent:eq(0)');
            var block = parent.find('.js-accord-content:eq(0)');
            if (!cur.hasClass('active')) {
                main_parent.find('.js-accord-link.active').not(cur).removeClass('active');
                main_parent.find('.js-accord-content').slideUp();
            }
            block.slideToggle();
            cur.toggleClass('active');
            e.preventDefault();
        });
    }
}
/* accordMulti */

/* accordSingle */
function accordSingle() {
    if ($('.js-accord-link').length) {
        $('.js-accord-link').on('click', function (e) {
            var cur = $(this);
            var parent = cur.parents('.js-accord-parent');
            var block = parent.find('.js-accord-content');
            block.slideToggle();
            cur.toggleClass('active');
            e.preventDefault();
        });
    }
}
/* accordSingle */

/*
 <div class="check-info js-accord-parent">
 <div class="check-info__title js-accord-link">

 </div>
 <div class="check-info__content js-accord-content">

 </div>
 </div>*/

/* tabs */
function tabs() {
    if ($('.js-tab-link').length) {
        var activeLink = $('.js-tab-link').parents('.current:eq(0)'),
            activeLinkData = activeLink.children('.js-tab-link').attr('data-tab'),
            activeBlock = $('#' + activeLinkData);
        activeBlock.fadeIn();
        $('.js-tab-link').on('click', function (e) {
            var cur = $(this);
            var cur_li = cur.parents('li:eq(0)');
            if (!cur_li.hasClass('current')) {
                cur_li.addClass('current').siblings('.current').removeClass('current');
                var cur_attr = cur.attr('data-tab');
                var block = $('#' + cur_attr);
                block.fadeIn().siblings('.js-tab').fadeOut(0);
            }
            e.preventDefault();
        });
    }
}
/* tabs end */

function fancybox() {
    // popup
    if ($('.js-popup-link').length) {
        var $link = $('.js-popup-link'),
            $close = $('.js-close-popup');
        var isSafari = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
        $link.on('click', function () {
            var href = $(this).attr('href'),
                linkData = $(this).data('tab');
            if (linkData) {
                var $popup = $(href),
                    $popupActiveLink = $popup.find('[data-tab="' + linkData + '"]'),
                    $popupActiveLinkParent = $popupActiveLink.parents('li:eq(0)'),
                    $popupActiveContent = $popup.find('#' + linkData);

                $popupActiveLinkParent.addClass('current').siblings('.current').removeClass('current');
                $popupActiveContent.fadeIn().siblings('.js-tab').fadeOut(0);
            }
            var safariSettings = {
                wrapCSS: 'fancybox-popup',
                padding: ['0', '0', '0', '0'],
                margin: [0, 0, 0, 0],
                overlayOpacity: 0,
                helpers: {
                    overlay: {
                        locked: false
                    }
                },
                beforeLoad: function () {
                    $('.wrapper').css({
                        'filter': 'blur(1px)',
                        '-webkit-filter': 'blur(1px)'
                    })
                },
                afterClose: function () {
                    $('.wrapper').css({
                        'filter': '',
                        '-webkit-filter': ''
                    })
                },
                beforeShow: function () {
                    $('body, html').addClass('hidden-safari');


                },
                beforeClose: function () {
                    $('body, html').removeClass('hidden-safari');

                }
            }
            var defaultSettings = {
                wrapCSS: 'fancybox-popup',
                padding: ['0', '0', '0', '0'],
                margin: [0, 0, 0, 0],
                overlayOpacity: 0,
                openMethod: 'zoomIn',
                helpers: {
                    overlay: {
                        locked: true
                    }
                },
                beforeLoad: function () {
                    $('.wrapper').css({
                        'filter': 'blur(1px)',
                        '-webkit-filter': 'blur(1px)'
                    })
                },
                afterClose: function () {
                    $('.wrapper').css({
                        'filter': '',
                        '-webkit-filter': ''
                    })
                }
            }

            if (isSafari) {
                $link.fancybox({
                    safariSettings
                });
            } else {
                $link.fancybox({
                    defaultSettings
                });
            }


        });
        // POPUP GALLERY SIMPLE PRODUCT
        $('.js-img-popup').each(function () {
            if (isSafari) {
                $(this).fancybox({
                    wrapCSS: 'fancybox-hidden',
                    padding: ['0', '0', '0', '0'],
                    margin: [0, 0, 0, 0],
                    scrolling: 'no',
                    scrollOutside: false,
                    overlayOpacity: 0,
                    helpers: {
                        overlay: {
                            locked: false
                        }
                    },

                    tpl: {
                        closeBtn: '<span class="popup-close"></span>'
                    },
                    beforeLoad: function () {
                        $('.wrapper').css({
                            'filter': 'blur(1px)',
                            '-webkit-filter': 'blur(1px)'
                        })
                    },
                    afterClose: function () {
                        $('.fancybox-hidden').removeClass('zoomOut');
                        $('.wrapper').css({
                            'filter': '',
                            '-webkit-filter': ''
                        })
                    },
                    beforeShow: function () {
                        $('.fancybox-hidden').addClass('zoomIn');

                        $('body, html').addClass('hidden-safari');

                        if ($('.gallery-product__main').length) {
                            $('.js-large-product__main').slick('unslick');
                            $('.js-large-product__main').slick({
                                infinite: false,
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                arrows: true,
                                prevArrow: '<span class="slick-prev"><svg class="icon-arrow-left"><use xlink:href="#arrow-tobottom"></use></svg></span>',
                                nextArrow: '<span class="slick-next"><svg class="icon-arrow-right"><use xlink:href="#arrow-tobottom"></use></svg></span>',
                                fade: true,
                                asNavFor: '.js-large-product__nav',
                                adaptiveHeight: false
                            });
                        }
                        if ($('.gallery-product__main').length) {
                            $('.js-large-product__nav').slick('unslick');
                            $('.js-large-product__nav').slick({
                                infinite: false,
                                variableWidth: true,
                                slidesToScroll: 1,
                                asNavFor: '.js-large-product__main',
                                arrows: true,
                                focusOnSelect: true,
                                prevArrow: '<span class="slick-prev"><svg class="icon-arrow-left"><use xlink:href="#arrow-tobottom"></use></svg></span>',
                                nextArrow: '<span class="slick-next"><svg class="icon-arrow-right"><use xlink:href="#arrow-tobottom"></use></svg></span>',
                            });


                        }
                        if ($('.gallery-product__main').length) {
                            var slider = $('.js-large-product__main'),
                                currentSlide = $('.gallery-product__main').find('.js-gallery-product__main').slick('slickCurrentSlide');

                            slider[0].slick.slickGoTo(currentSlide);
                        }

                    },
                    beforeClose: function () {
                        $('.fancybox-hidden').addClass('zoomOut');
                        $('body, html').removeClass('hidden-safari');

                        if ($('.gallery-product__main').length) {
                            var slider = $('.gallery-product__main').find('.js-gallery-product__main'),
                                currentSlide = $('.js-large-product__main').slick('slickCurrentSlide');

                            slider[0].slick.slickGoTo(currentSlide);
                        }
                    }
                });
            } else {
                $(this).fancybox({
                    wrapCSS: 'fancybox-hidden',
                    padding: ['0', '0', '0', '0'],
                    margin: [0, 0, 0, 0],
                    scrolling: 'no',
                    scrollOutside: false,
                    overlayOpacity: 0,
                    helpers: {
                        overlay: {
                            locked: true
                        }
                    },

                    tpl: {
                        closeBtn: '<span class="popup-close"></span>'
                    },
                    beforeLoad: function () {
                        $('.wrapper').css({
                            'filter': 'blur(1px)',
                            '-webkit-filter': 'blur(1px)'
                        })
                    },
                    beforeShow: function () {

                        $('.fancybox-hidden').addClass('zoomIn');

                        if ($('.gallery-product__main').length) {
                            $('.js-large-product__main').slick('unslick');
                            $('.js-large-product__main').slick({
                                infinite: false,
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                arrows: true,
                                fade: true,
                                asNavFor: '.js-large-product__nav',
                                adaptiveHeight: true,
                                prevArrow: '<span class="slick-prev"><svg class="icon-arrow-left"><use xlink:href="#arrow-tobottom"></use></svg></span>',
                                nextArrow: '<span class="slick-next"><svg class="icon-arrow-right"><use xlink:href="#arrow-tobottom"></use></svg></span>',


                            });
                        }
                        if ($('.gallery-product__main').length) {
                            $('.js-large-product__nav').slick('unslick');
                            $('.js-large-product__nav').slick({
                                infinite: false,
                                // slidesToShow: 5,
                                variableWidth: true,
                                slidesToScroll: 1,
                                asNavFor: '.js-large-product__main',
                                arrows: true,
                                focusOnSelect: true,
                                prevArrow: '<span class="slick-prev"><svg class="icon-arrow-left"><use xlink:href="#arrow-tobottom"></use></svg></span>',
                                nextArrow: '<span class="slick-next"><svg class="icon-arrow-right"><use xlink:href="#arrow-tobottom"></use></svg></span>',

                            });
                        }
                        if ($('.gallery-product__main').length) {
                            var slider = $('.js-large-product__main'),
                                currentSlide = $('.gallery-product__main').find('.js-gallery-product__main').slick('slickCurrentSlide');

                            slider[0].slick.slickGoTo(currentSlide);
                        }
                    },
                    beforeClose: function () {
                        $('.fancybox-hidden').addClass('zoomOut');
                        if ($('.gallery-product__main').length) {
                            var slider = $('.gallery-product__main').find('.js-gallery-product__main'),
                                currentSlide = $('.js-large-product__main').slick('slickCurrentSlide');

                            slider[0].slick.slickGoTo(currentSlide);
                        }

                    },
                    afterClose: function () {
                        $('.fancybox-hidden').removeClass('zoomOut');
                        $('.wrapper').css({
                            'filter': '',
                            '-webkit-filter': ''
                        })
                    }
                });
            }
        })


        //POPPU GALLERY MODULE PRODUCTS
        $('.js-img-popup-module').each(function () {
            if (isSafari) {
                $(this).fancybox({
                    wrapCSS: 'fancybox-module fancybox-hidden',
                    padding: ['0', '0', '0', '0'],
                    margin: [0, 0, 0, 0],
                    scrolling: 'no',
                    scrollOutside: false,
                    overlayOpacity: 0,
                    arrows: true,

                    helpers: {
                        overlay: {
                            locked: false
                        },
                        thumbs: {
                            width: 50,
                            height: 50
                        }
                    },

                    tpl: {
                        closeBtn: '<span class="popup-close"></span>'
                    },
                    beforeLoad: function () {
                        $('.wrapper').css({
                            'filter': 'blur(1px)',
                            '-webkit-filter': 'blur(1px)'
                        })
                    },
                    afterClose: function () {
                        $('.wrapper').css({
                            'filter': '',
                            '-webkit-filter': ''
                        })
                        $('.fancybox-hidden').removeClass('zoomOut');
                    },
                    beforeShow: function () {
                        $('.fancybox-hidden').addClass('zoomIn');
                        $('body, html').addClass('hidden-safari');


                        if ($('.js-large-product-module__main').length) {
                            $('.js-large-product-module__main').slick('unslick');
                            $('.js-large-product-module__main').slick({
                                infinite: false,
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                arrows: true,
                                fade: true,
                                asNavFor: '.js-large-product-module__nav',
                                adaptiveHeight: true,
                                prevArrow: '<span class="slick-prev"><svg class="icon-arrow-left"><use xlink:href="#arrow-tobottom"></use></svg></span>',
                                nextArrow: '<span class="slick-next"><svg class="icon-arrow-right"><use xlink:href="#arrow-tobottom"></use></svg></span>',
                            });
                        }

                        if ($('.js-large-product-module__nav').length) {
                            $('.js-large-product-module__nav').slick('unslick');
                            $('.js-large-product-module__nav').slick({
                                infinite: false,
                                variableWidth: true,
                                slidesToScroll: 1,
                                asNavFor: '.js-large-product-module__main',
                                arrows: true,
                                focusOnSelect: true,
                                prevArrow: '<span class="slick-prev"><svg class="icon-arrow-left"><use xlink:href="#arrow-tobottom"></use></svg></span>',
                                nextArrow: '<span class="slick-next"><svg class="icon-arrow-right"><use xlink:href="#arrow-tobottom"></use></svg></span>',
                            });

                            var slider = $('.js-large-product-module__main'),
                                currentSlide = $('.gallery-product__main').find('.js-gallery-product__main').slick('slickCurrentSlide');

                            slider[0].slick.slickGoTo(currentSlide);
                        }

                    },
                    beforeClose: function () {
                        $('.fancybox-hidden').addClass('zoomOut');
                        $('body, html').removeClass('hidden-safari');
                        if ($('.js-gallery-product__main').length) {
                            var slider = $('.gallery-product__main').find('.js-gallery-product__main'),
                                currentSlide = $('.js-large-product-module__main').slick('slickCurrentSlide');

                            slider[0].slick.slickGoTo(currentSlide);
                        }
                    }
                });
            } else {
                $(this).fancybox({
                    wrapCSS: 'fancybox-module fancybox-hidden',
                    padding: ['0', '0', '0', '0'],
                    margin: [0, 0, 0, 0],
                    scrolling: 'no',
                    scrollOutside: false,
                    arrows: true,

                    helpers: {
                        overlay: {
                            locked: true
                        },
                        thumbs: {
                            width: 50,
                            height: 50
                        }
                    },

                    tpl: {
                        closeBtn: '<span class="popup-close"></span>'
                    },
                    beforeLoad: function () {
                        $('.wrapper').css({
                            'filter': 'blur(1px)',
                            '-webkit-filter': 'blur(1px)'
                        })
                    },
                    beforeShow: function () {
                        $('.fancybox-hidden').addClass('zoomIn');
                        if ($('.js-large-product-module__main').length) {
                            $('.js-large-product-module__main').slick('unslick');
                            $('.js-large-product-module__main').slick({
                                infinite: false,
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                arrows: true,
                                fade: true,
                                asNavFor: '.js-large-product-module__nav',
                                adaptiveHeight: true,
                                prevArrow: '<span class="slick-prev"><svg class="icon-arrow-left"><use xlink:href="#arrow-tobottom"></use></svg></span>',
                                nextArrow: '<span class="slick-next"><svg class="icon-arrow-right"><use xlink:href="#arrow-tobottom"></use></svg></span>',
                            });
                        }

                        if ($('.js-large-product-module__nav').length) {
                            $('.js-large-product-module__nav').slick('unslick');
                            $('.js-large-product-module__nav').slick({
                                infinite: false,
                                variableWidth: true,
                                slidesToScroll: 1,
                                asNavFor: '.js-large-product-module__main',
                                arrows: true,
                                focusOnSelect: true,
                                prevArrow: '<span class="slick-prev"><svg class="icon-arrow-left"><use xlink:href="#arrow-tobottom"></use></svg></span>',
                                nextArrow: '<span class="slick-next"><svg class="icon-arrow-right"><use xlink:href="#arrow-tobottom"></use></svg></span>',
                            });

                            var slider = $('.js-large-product-module__main'),
                                currentSlide = $('.gallery-product__main').find('.js-gallery-product__main').slick('slickCurrentSlide');

                            slider[0].slick.slickGoTo(currentSlide);
                        }


                    },
                    afterClose: function () {
                        $('.fancybox-hidden').removeClass('zoomOut');
                        $('.wrapper').css({
                            'filter': '',
                            '-webkit-filter': ''
                        })
                    },
                    beforeClose: function () {
                        $('.fancybox-hidden').addClass('zoomOut');
                        if ($('.js-gallery-product__main').length) {
                            var slider = $('.gallery-product__main').find('.js-gallery-product__main'),
                                currentSlide = $('.js-large-product-module__main').slick('slickCurrentSlide');

                            slider[0].slick.slickGoTo(currentSlide);
                        }

                    }
                });
            }
        })


        $close.on('click', function (e) {
            e.preventDefault();
            $.fancybox.close();
            $('.fancybox-hidden').addClass('zoomOut');
        });
    }
    if($('.js-cbform').length) {
        var $close = $('.d-popup__close');

        $('.js-cbform').fancybox({
            btnSmall: false
        });


        $close.on('click', function (e) {
            e.preventDefault();
            $.fancybox.close();
        });
    }
    // image

}
/* fancybox end */

// addPositionClass
function addPositionClass(position, feedback, obj) {
    removePositionClass(obj);
    obj.css(position).addClass(feedback.vertical).addClass(feedback.horizontal);
}

// removePositionClass
function removePositionClass(obj) {
    obj.removeClass('top bottom center left right');
}

/* UI MULTISELECT */
/* default Select */
selectConfigDefault = {
    header: false,
    height: 'auto',
    minWidth: 'auto',
    classes: 'select',
    selectedList: 1,
    multiple: false,
    position: {
        my: 'left top+1',
        at: 'left bottom+1',
        collision: 'flip flip',
        using: function (position, feedback) {
            addPositionClass(position, feedback, $(this));
        }
    },
    arrow: true,
    divider: false,
    corner: false,
    icon: false,
    jscrollpane: false,
    filter: false,
    filterOptions: {},
    dataImg: false    /*<option value="1" data-img="pic/lang/ua.png">value 1</option>*/
}

/* customSelect */
function customSelect(objSelName, arrConfig) {
    if (objSelName.length) {
        objSelName.each(function () {
            var self = $(this);
            var curClass = '';

            // adding class
            if (self.is('[class]'))
                curClass = self.attr('class');
            var placeholderFlag, noneSelectedText, cur_option_placeholder_index;

            // placeholder
            if (self.find('option[selected]').length) {
                var cur_option_selected = self.find('option[selected]');
                if (cur_option_selected.attr('data-placeholder')) {
                    placeholderFlag = true;
                    var cur_option_placeholder_index = cur_option_selected.index();
                } else
                    placeholderFlag = false;
            } else
                placeholderFlag = false;


            // init
            self.multiselect({
                header: arrConfig.header,
                height: arrConfig.height,
                minWidth: arrConfig.minWidth,
                classes: arrConfig.classes + ' ' + curClass,
                checkAllText: arrConfig.checkAllText,
                uncheckAllText: arrConfig.uncheckAllText,
                noneSelectedText: arrConfig.noneSelectedText,
                selectedText: arrConfig.selectedText,
                selectedList: arrConfig.selectedList,
                show: arrConfig.show,
                hide: arrConfig.hide,
                autoOpen: arrConfig.autoOpen,
                multiple: arrConfig.multiple,
                position: arrConfig.position,
                appendTo: arrConfig.appendTo,

                create: function (event, ui) {
                    var
                        btn = $(this).multiselect('getButton'),
                        btnIcon = btn.find('.ui-icon'),
                        widg = $(this).multiselect('widget');
                    if (placeholderFlag) {
                        btn.addClass('ui-state-placeholder');
                    }
                    btn.find('span').not('[class]').addClass('ui-multiselect-value');

                    // button divider
                    if (arrConfig.divider === true)
                        btn.append('<span class="ui-multiselect-divider"></span>');

                    // button arrow
                    if (arrConfig.arrow === true)
                        btn.append('<span class="ui-multiselect-arrow"></span>');

                    // button icon
                    if (arrConfig.icon !== true)
                        btnIcon.remove();
                    else
                        btnIcon.removeClass('ui-icon ui-icon-triangle-2-n-s').addClass('ui-multiselect-icon');
                    btn.children().wrapAll('<span class="ui-multiselect-inner"></span>');
                    widg.children().wrapAll('<div class="ui-multiselect-menu-inner"></div>');

                    // widget scrollpane

                    widg.find('.ui-multiselect-checkboxes').wrap('<div class="ui-multiselect-wrap-scrollpane"><div class="ui-multiselect-scrollpane"></div></div>');


                    // widget corner
                    if (arrConfig.corner === true)
                        widg.append('<div class="ui-multiselect-corner"></div>');

                    // button image
                    if (arrConfig.dataImg === true) {
                        var
                            listOptions = $(this).find('option'),
                            list = widg.find('.ui-multiselect-checkboxes li span');
                        list.each(function (i) {
                            $(this).html('<img src="' + listOptions.eq(i).attr('data-img') + '" />');
                            if (listOptions.eq(i).is(':selected')) {
                                btn.find('.ui-multiselect-value').html('<img src="' + listOptions.eq(i).attr('data-img') + '" />');
                            }
                        });
                        /*ie7-8 image click bug*/
                        list.on('click', function () {
                            $(this).parents('li').find('input').trigger('click');
                        });
                    }

                    // check all
                    widg.on('click', '.ui-multiselect-all', function () {
                        btn.removeClass('ui-state-placeholder');
                    });

                    // uncheck all
                    widg.on('click', '.ui-multiselect-none', function () {
                        btn.addClass('ui-state-placeholder');
                    });

                    // SELECT WITH CURRENCY
                    if (btn.hasClass('select-currency')) {
                        var cur_option = $(this).find('option[selected]');
                        btn.find('.ui-multiselect-inner').prepend('<div class="ui-multiselect-currency-label"></div>');

                        // data-currency-symbol
                        if (cur_option.attr('data-currency-symbol')) {
                            cur_option_symbol = cur_option.attr('data-currency-symbol');
                            btn.find('.ui-multiselect-currency-label').append('<span class="valuta-blue-round">' + cur_option_symbol + '</span>');
                        }

                        // data-currency-sum
                        if (cur_option.attr('data-currency-sum')) {
                            cur_option_sum = cur_option.attr('data-currency-sum');
                            btn.find('.ui-multiselect-currency-label').append('<span class="valuta-sum">' + cur_option_sum + '</span>');
                        }

                        // data-currency-text
                        if (cur_option.attr('data-currency-text')) {
                            cur_option_text = cur_option.attr('data-currency-text');
                            btn.find('.ui-multiselect-currency-label').append('<span class="valuta-text">' + cur_option_text + '</span>');
                        }
                    }
                },
                open: function (event, ui) {
                    var btn = $(this).multiselect('getButton');
                    var widg = $(this).multiselect('widget');
                    widg.find('.ui-multiselect-checkboxes li:eq(' + cur_option_placeholder_index + ')').addClass('ui-multiselect-placeholder');

                    // fix scroll drop list
                    var list = widg.find('.ui-multiselect-checkboxes');
                    var maxH = parseInt(list.css('max-height'));
                    if (maxH > parseInt(list.height()))
                        list.removeClass('list-fix-scroll');
                    else
                        list.addClass('list-fix-scroll');

                    // jscrollpane run handler

                    scrollbarVertical(widg.find('.ui-multiselect-scrollpane'));


                    // SELECT WITH CURRENCY
                    if (btn.hasClass('select-currency') && !widg.find('.ui-multiselect-currency-label').length) {
                        $(this).find('option').each(function () {
                            var cur_option = $(this);
                            var cur_option_index = cur_option.index();
                            var cur_widg_label = widg.find('.ui-multiselect-checkboxes li:eq(' + cur_option_index + ') label');
                            cur_widg_label.prepend('<div class="ui-multiselect-currency-label"></div>');

                            // data-currency-symbol
                            if (cur_option.attr('data-currency-symbol')) {
                                cur_option_symbol = cur_option.attr('data-currency-symbol');
                                cur_widg_label.find('.ui-multiselect-currency-label').append('<span class="valuta-blue-round">' + cur_option_symbol + '</span>');
                            }

                            // data-currency-sum
                            if (cur_option.attr('data-currency-sum')) {
                                cur_option_sum = cur_option.attr('data-currency-sum');
                                cur_widg_label.find('.ui-multiselect-currency-label').append('<span class="valuta-sum">' + cur_option_sum + '</span>');
                            }

                            // data-currency-text
                            if (cur_option.attr('data-currency-text')) {
                                cur_option_text = cur_option.attr('data-currency-text');
                                cur_widg_label.find('.ui-multiselect-currency-label').append('<span class="valuta-text">' + cur_option_text + '</span>');
                            }
                        });
                    }
                },
                click: function (event, ui) {
                    var
                        btn = $(this).multiselect('getButton'),
                        widg = $(this).multiselect('widget'),
                        flagCheck = false;

                    // placeholder
                    if (widg.find('.ui-multiselect-checkboxes :checked').parents('.ui-multiselect-placeholder').length) {
                        btn.addClass('ui-state-placeholder');
                    } else {
                        btn.removeClass('ui-state-placeholder');
                    }

                    // SELECT WITH CURRENCY
                    if (btn.hasClass('select-currency')) {
                        var cur_option_index = widg.find('.ui-multiselect-checkboxes :checked').parents('li:eq(0)').index();
                        var cur_option = $(this).find('option:eq(' + cur_option_index + ')');
                        btn.find('.ui-multiselect-currency-label').html('');

                        // data-currency-symbol
                        if (cur_option.attr('data-currency-symbol')) {
                            cur_option_symbol = cur_option.attr('data-currency-symbol');
                            btn.find('.ui-multiselect-currency-label').append('<span class="valuta-blue-round">' + cur_option_symbol + '</span>');
                        }

                        // data-currency-sum
                        if (cur_option.attr('data-currency-sum')) {
                            cur_option_sum = cur_option.attr('data-currency-sum');
                            btn.find('.ui-multiselect-currency-label').append('<span class="valuta-sum">' + cur_option_sum + '</span>');
                        }

                        // data-currency-text
                        if (cur_option.attr('data-currency-text')) {
                            cur_option_text = cur_option.attr('data-currency-text');
                            btn.find('.ui-multiselect-currency-label').append('<span class="valuta-text">' + cur_option_text + '</span>');
                        }
                    }
                },
                beforeclose: function (event, ui) {
                    var widg = $(this).multiselect('widget');
                    removePositionClass(widg);
                    /* jscrollpane destroy handler */

                    var jscrollpane = widg.find('.ui-multiselect-scrollpane').data('jsp');
                    if (jscrollpane) {
                        jscrollpane.destroy();
                    }
                    $(document).unbind('mousewheel.false');

                }
            });
            /* filter options */
            if (arrConfig.filter === true) {
                self.multiselectfilter(arrConfig.filterOptions);
            }
            /* filter options end */
        });

        $('.ui-multiselect-checkboxes input[type="checkbox"]').on('click', function () {
            $(this).closest('label').toggleClass('checked');
        });


    }
}
/* customSelect end */

// customSelectClose
function customSelectClose(objSelect) {
    if (objSelect.length) {
        objSelect.each(function () {
            var self = $(this);
            if (self.next('button.ui-multiselect').length)
                self.multiselect('close');
        });
    }
}

// customSelectRefresh
function customSelectRefresh(objSelect) {
    if (objSelect.length) {
        objSelect.each(function () {
            var self = $(this);
            if (self.next('button.ui-multiselect').length)
                self.multiselect('refresh');
        });
    }
}
/* UI MULTISELECT END */

/* SCROLLPANE */
/* scrollbarVertical */
function scrollbarVertical(objScroll) {
    if (objScroll.length) {
        objScroll.each(function () {
            $(this).jScrollPane({
                mouseWheelSpeed: 100,
                showArrows: false,
                arrowScrollOnHover: false,
                verticalDragMinHeight: 40,
                horizontalDragMinWidth: 0,
                horizontalDragMaxWidth: 0,
                autoReinitialise: true
            });
        });
    }
}
/* scrollbarVertical end */

/* scrollbarHorizontal */
function scrollbarHorizontal(objScroll) {
    if (objScroll.length) {
        objScroll.each(function () {
            $(this).jScrollPane({
                verticalDragMaxHeight: 0,
                verticalDragMinHeight: 0,
                horizontalDragMinWidth: 64,
                horizontalDragMaxWidth: 80,
                autoReinitialise: true
            });
        });
    }
}
/* scrollbarHorizontal end */

/* SCROLLPANE END */

/*YANDEX MAP*/
function map() {
    if ($('#js-map').length) {
        jQuery.getScript("http://api-maps.yandex.ru/2.0/?load=package.full&amp;lang=ru-RU", function () {
            var myMap;

            function init() {
                myMap = new ymaps.Map("js-map", {
                    center: [55.755814, 37.617635],
                    zoom: 12
                });
                var myPlacemark = new ymaps.Placemark([55.778699, 37.512371], {
                    balloonContent: '<div class="map-baloon"><div class="map-baloon__row"><h2 class="map-baloon__title">Полежаевская</h2></div><div class="map-baloon__row"><div class="map-baloon__subtitle">Адрес</div><div class="map-baloon__content">м. Полежаевская, ул. Зорге, д.1, стр. 2,<br/>ТЦ "Дом для Дома", 2-й этаж</div></div><div class="map-baloon__row"><div class="map-baloon__subtitle">График работы</div><div class="map-baloon__content">10:00 до 20:00 без выходных</div></div><div class="map-baloon__row"><div class="map-baloon__subtitle">Телефоны</div><div class="map-baloon__content"><span class="map-baloon__phone">+7 (495) 740-28-43</span><span class="map-baloon__phone">+7 (495) 255-18-10</span></div></div></div>'
                }, {
                    iconImageHref: 'img/map-pin.svg',
                    iconImageSize: [49, 61],
                    hideIconOnBalloonOpen: false
                });

                var myPlacemark2 = new ymaps.Placemark([55.761051, 37.650697], {
                    balloonContent: '<div class="map-baloon"><div class="map-baloon__row"><h2 class="map-baloon__title">Полежаевская</h2></div><div class="map-baloon__row"><div class="map-baloon__subtitle">Адрес</div><div class="map-baloon__content">м. Полежаевская, ул. Зорге, д.1, стр. 2,<br/>ТЦ "Дом для Дома", 2-й этаж</div></div><div class="map-baloon__row"><div class="map-baloon__subtitle">График работы</div><div class="map-baloon__content">10:00 до 20:00 без выходных</div></div><div class="map-baloon__row"><div class="map-baloon__subtitle">Телефоны</div><div class="map-baloon__content"><span class="map-baloon__phone">+7 (495) 740-28-43</span><span class="map-baloon__phone">+7 (495) 255-18-10</span></div></div></div>'
                }, {
                    iconImageHref: 'img/map-pin.svg',
                    iconImageSize: [49, 61],
                    hideIconOnBalloonOpen: false
                });

                var myPlacemark3 = new ymaps.Placemark([55.756212, 37.583406], {
                    balloonContent: '<div class="map-baloon"><div class="map-baloon__row"><h2 class="map-baloon__title">Полежаевская</h2></div><div class="map-baloon__row"><div class="map-baloon__subtitle">Адрес</div><div class="map-baloon__content">м. Полежаевская, ул. Зорге, д.1, стр. 2,<br/>ТЦ "Дом для Дома", 2-й этаж</div></div><div class="map-baloon__row"><div class="map-baloon__subtitle">График работы</div><div class="map-baloon__content">10:00 до 20:00 без выходных</div></div><div class="map-baloon__row"><div class="map-baloon__subtitle">Телефоны</div><div class="map-baloon__content"><span class="map-baloon__phone">+7 (495) 740-28-43</span><span class="map-baloon__phone">+7 (495) 255-18-10</span></div></div></div>'
                }, {
                    iconImageHref: 'img/map-pin.svg',
                    iconImageSize: [49, 61],
                    hideIconOnBalloonOpen: false
                });

                var myPlacemark4 = new ymaps.Placemark([55.716696, 37.608812], {
                    balloonContent: '<div class="map-baloon"><div class="map-baloon__row"><h2 class="map-baloon__title">Полежаевская</h2></div><div class="map-baloon__row"><div class="map-baloon__subtitle">Адрес</div><div class="map-baloon__content">м. Полежаевская, ул. Зорге, д.1, стр. 2,<br/>ТЦ "Дом для Дома", 2-й этаж</div></div><div class="map-baloon__row"><div class="map-baloon__subtitle">График работы</div><div class="map-baloon__content">10:00 до 20:00 без выходных</div></div><div class="map-baloon__row"><div class="map-baloon__subtitle">Телефоны</div><div class="map-baloon__content"><span class="map-baloon__phone">+7 (495) 740-28-43</span><span class="map-baloon__phone">+7 (495) 255-18-10</span></div></div></div>'
                }, {
                    iconImageHref: 'img/map-pin.svg',
                    iconImageSize: [49, 61],
                    hideIconOnBalloonOpen: false
                });

                myMap.geoObjects.add(myPlacemark);
                myMap.geoObjects.add(myPlacemark2);
                myMap.geoObjects.add(myPlacemark3);
                myMap.geoObjects.add(myPlacemark4);

                myPlacemark.events
                    .add('mouseenter', function (e) {
                        e.get('target').options.set({iconImageHref: 'img/map-pin-orange.svg'});
                    })
                    .add('mouseleave', function (e) {
                        e.get('target').options.set({iconImageHref: 'img/map-pin.svg'});
                    });
                myPlacemark2.events
                    .add('mouseenter', function (e) {
                        e.get('target').options.set({iconImageHref: 'img/map-pin-orange.svg'});
                    })
                    .add('mouseleave', function (e) {
                        e.get('target').options.set({iconImageHref: 'img/map-pin.svg'});
                    });
                myPlacemark3.events
                    .add('mouseenter', function (e) {
                        e.get('target').options.set({iconImageHref: 'img/map-pin-orange.svg'});
                    })
                    .add('mouseleave', function (e) {
                        e.get('target').options.set({iconImageHref: 'img/map-pin.svg'});
                    });
                myPlacemark4.events
                    .add('mouseenter', function (e) {
                        e.get('target').options.set({iconImageHref: 'img/map-pin-orange.svg'});
                    })
                    .add('mouseleave', function (e) {
                        e.get('target').options.set({iconImageHref: 'img/map-pin.svg'});
                    });
            }

            ymaps.ready(init);
        });
    }
}
/*YANDEX MAP END*/

/*filter drop*/
function filterDrop() {

    //FILTER DROPS HIDE/SHOW
    if ($('.js-filter-btn').length) {
        $('.js-filter-btn').on('click', function (e) {
            var cur = $(this);
            $(document).unbind('click.drop');
            $('.js-filter-drop').slideUp(100);
            var jscrollpane = $('.js-filter-drop-parent').find($('.js-scroll')).data('jsp');
            if (jscrollpane) {
                jscrollpane.destroy();
            }
            $('.js-filter-drop-parent').find($('.select-block-drop__inner')).removeClass('js-scroll');

            $('.js-filter-drop-parent').find($('.checkboxes__inner')).removeClass('js-scroll');


            $('.js-filter-btn').not(cur).removeClass('active');
            if (!cur.hasClass('active')) {
                var yourClick = true;
                var drop = cur.parents('.js-filter-drop-parent:eq(0)').find('.js-filter-drop:eq(0)');

                drop.slideDown(100);

                cur.parents('.js-filter-drop-parent').find($('.select-block-drop__inner')).addClass('js-scroll');
                if ($(window).width() > 1025) {
                    cur.parents('.js-filter-drop-parent').find($('.checkboxes__inner')).addClass('js-scroll');
                }

                scrollbarVertical($('.js-scroll'));

                cur.addClass('active');

                $(document).bind('click.drop', function (e) {
                    if (!yourClick && !$(e.target).closest(drop).length) {
                        drop.slideUp(100);
                        var jscrollpane = $('.js-filter-drop-parent').find($('.js-scroll')).data('jsp');
                        if (jscrollpane) {
                            jscrollpane.destroy();
                        }
                        $('.js-filter-drop-parent').find($('.select-block-drop__inner')).removeClass('js-scroll');
                        $('.js-filter-drop-parent').find($('.checkboxes__inner')).removeClass('js-scroll');
                        $('.js-filter-btn').removeClass('active');
                        $(document).unbind('click.drop');
                    }
                    yourClick = false;
                });
            } else {
                cur.removeClass('active');
                $('.js-filter-drop').slideUp(100);
                var jscrollpane = $('.js-filter-drop-parent').find($('.js-scroll')).data('jsp');
                if (jscrollpane) {
                    jscrollpane.destroy();
                }
                $('.js-filter-drop-parent').find($('.select-block-drop__inner')).removeClass('js-scroll');
                $('.js-filter-drop-parent').find($('.checkboxes__inner')).removeClass('js-scroll');
            }
            e.preventDefault();
        });
        $(document).on('click', function (e) {
            var target = $(e.target);
            var btn = !!target.closest($('.js-filter-btn')).length;
            var cur_block = !!target.closest($(".js-filter-drop")).length;
            var cross = !!target.closest($('.js-filter-btn-cross')).length;
            if (!btn && !cur_block && !cross && $(".js-filter-drop").is('visible')) {
                $('.js-filter-btn').removeClass('active');
                $('.js-filter-drop').slideUp(100);
                var jscrollpane = $('.js-filter-drop-parent').find($('.js-scroll')).data('jsp');
                if (jscrollpane) {
                    jscrollpane.destroy();
                }
                $('.js-filter-drop-parent').find($('.select-block-drop__inner')).removeClass('js-scroll');
                $('.js-filter-drop-parent').find($('.checkboxes__inner')).removeClass('js-scroll');
            }

        });
    }

    //FILTER DROPS DESTROY SCROLLPANE ON TABLET
    if ($(window).width() < 1024) {
        $('.js-filter-drop').each(function () {
            var jscrollpane = $(this).find('.js-scroll').data('jsp');
            if (jscrollpane) {
                jscrollpane.destroy();
            }
        })

    }
    $(window).resize(function () {
        if ($(window).width() < 1024) {
            $('.js-filter-drop').each(function () {
                var jscrollpane = $(this).find('.js-scroll').data('jsp');
                if (jscrollpane) {
                    jscrollpane.destroy();
                }
            })

        }
    })

    // SELECT CHECKBOXES
    if ($('.js-checkbox').length && $(window).width() > 1023) {

        $('.js-checkbox').on('click', function () {
            var cur = $(this),
                holder = '.js-filter-drop-parent',
                active_text = $('.js-filter-active-text'),
                hide_text = $('.js-filter-placeholder'),
                btn = $('.js-filter-btn'),
                cross = $('.js-filter-btn-cross'),
                arrow = $('.js-filter-btn-arrow'),
                drop = '.js-filter-drop';

            cur.parents(holder).find(hide_text).hide();
            cur.parents(holder).find(active_text).show();
            cur.parents(holder).find(btn).addClass('js-full');
            cur.parents(holder).find(arrow).addClass('hidden');
            cur.parents(holder).find(cross).addClass('active');
            cur.parents(drop).find($('.js-filter-action')).addClass('checked');
        })

        // INPUT CHECKED COUNTER

    }
    if ($(window).width() < 1023) {
        $('.js-checkbox').on('click', function () {
            var cur = $(this),
                holder = '.js-filter-drop-parent',
                active_text = $('.js-filter-active-text'),
                hide_text = $('.js-filter-placeholder'),
                btn = $('.js-filter-btn'),
                cross = $('.js-filter-btn-cross'),
                arrow = $('.js-filter-btn-arrow'),
                drop = '.js-filter-drop';
            cur.parents(drop).find($('.js-filter-action')).addClass('checked');
        })
    }

    // RESET SELECT
    if ($('.js-filter-btn-cross').length) {
        $('.js-filter-btn-cross').on('click', function (e) {

            var cur = $(this),
                holder = '.js-filter-drop-parent',
                active_text = $('.js-filter-active-text'),
                hide_text = $('.js-filter-placeholder'),
                btn = $('.js-filter-btn'),
                cross = $('.js-filter-btn-cross'),
                arrow = $('.js-filter-btn-arrow');

            cur.parents(holder).find(hide_text).show();
            cur.parents(holder).find(active_text).hide();
            cur.parents(holder).find(btn).removeClass('js-full');
            cur.parents(holder).find(arrow).removeClass('hidden');
            cur.parents(holder).find(cross).removeClass('active');
            cur.parents(holder).find($('input[type="checkbox"]:checked')).removeAttr('checked');
        })
    }
    if ($('.js-filter-reset').length) {
        $('.js-filter-reset').on('click', function () {
            var cur = $(this),
                holder = '.js-filter-drop-parent',
                active_text = $('.js-filter-active-text'),
                hide_text = $('.js-filter-placeholder'),
                btn = $('.js-filter-btn'),
                cross = $('.js-filter-btn-cross'),
                arrow = $('.js-filter-btn-arrow');
            cur.parents(holder).find(hide_text).show();
            cur.parents(holder).find(active_text).hide();
            cur.parents(holder).find(btn).removeClass('js-full');
            cur.parents(holder).find(arrow).removeClass('hidden');
            cur.parents(holder).find(cross).removeClass('active');
            cur.parents('.js-filter-action').removeClass('checked');
        })
    }


    if ($('.js-drop-select-text').length) {
        $('.js-drop-select-text').on('click', function () {
            var cur = $(this),
                placeholder = $('.js-drop-placeholder'),
                parent = '.js-filter-drop-parent',
                text = cur.find($('.select-block-drop__text')).html(),
                drop = cur.parents(parent).find($('.js-filter-drop')),
                btn = cur.parents(parent).find($('.js-filter-btn'));

            placeholder_one = $('.js-drop-placeholder-one');
            placeholder_two = $('.js-drop-placeholder-two');

            text_one = cur.find($('.js-drop-select-text-one')).html();
            text_two = cur.find($('.js-drop-select-text-two')).html();

            cur.parents(parent).find(placeholder_one).text(text_one);
            cur.parents(parent).find(placeholder_two).text(text_two);

            cur.parents(parent).find(placeholder).text(text);
            drop.slideUp(200);
            var jscrollpane = $('.js-filter-drop-parent').find($('.js-scroll')).data('jsp');
            if (jscrollpane) {
                jscrollpane.destroy();
            }
            $('.js-filter-drop-parent').find($('.select-block-drop__inner')).removeClass('js-scroll');
            btn.removeClass('active');


        })
    }
	
	if ($('.js-drop-select-text').length) {
        $('.js-drop-select-text').on('click', function () {
			var img_cont_new = $('#color_pic');
			var src_img_new = $(this).find($('img')).attr('src');
			img_cont_new.attr('src', src_img_new);
		})
    }

    if ($('.js-filter-scroll').length) {
        var offset = $('.js-filter-scroll').offset().top;
        $(window).on('scroll', function () {
            var scroll = $(window).scrollTop();
            if (offset < $(window).scrollTop()) {
                $('.js-filter-scroll').addClass('sticky');
            } else {
                $('.js-filter-scroll').removeClass('sticky');
            }
        })
    }
    if ($('.js-scroll-sort').length) {
        var offset_sort = $('.js-scroll-sort').offset().top;
        $(window).on('scroll', function () {
            var scroll = $(window).scrollTop();
            if (offset_sort < $(window).scrollTop() + 66) {
                $('.js-scroll-sort').addClass('sticky');
            } else {
                $('.js-scroll-sort').removeClass('sticky');
            }
        })
    }
}
function filterTablet() {
    // DROPS ON TABLET
    if ($('.js-filter-btn-r').length) {

        $('.js-filter-btn-r').on('click', function (e) {
            var cur = $(this);
            $('.js-filter-drop-r').removeClass('active');
            $('.js-filter-btn-r').not(cur).removeClass('active');
            if (!cur.hasClass('active')) {
                var drop = cur.parents('.js-filter-drop-parent-r:eq(0)').find('.js-filter-drop-r:eq(0)');
                drop.addClass('active');
                cur.addClass('active');

            } else {
                cur.removeClass('active');
                $('.js-filter-drop-r').removeClass('active');
            }
            e.preventDefault();
        });
        $(document).on('click', function (e) {
            var target = $(e.target);
            var btn = !!target.closest($('.js-filter-btn-r')).length;
            var cur_block = !!target.closest($(".js-filter-drop-r")).length;

            if (!btn && !cur_block && $(".js-filter-drop-r").is(':visible')) {
                $('.js-filter-btn-r').removeClass('active');
                $('.js-filter-drop-r').removeClass('active');
            }

        });
    }

}
/*filter drop end*/

var filtersProduct = (function () {

    var filters = {

        writeChanges: function ($this) {
            var parentBlock = $this.parents('.js-filter-drop-parent');
            var allCheckbox = parentBlock.find('input[type="checkbox"]');
            var link = parentBlock.find('.js-filter-btn');
            var linkVal = link.find('.js-checkboxes-amount');
            var resetLink = parentBlock.find('.js-filter-btn-cross');
            var arrow = parentBlock.find('.js-filter-btn-arrow');
            var active_text = parentBlock.find('.js-filter-active-text');
            var placeholder = parentBlock.find('.js-filter-placeholder');
            var j = 0;

            allCheckbox.each(function (i, val) {

                if ($(this).is(':checked')) {
                    j++;
                    linkVal.text(j);
                }
            });
            if (!allCheckbox.is(':checked')) {
                active_text.hide();
                placeholder.show();
                linkVal.text('');

                link.removeClass('js-full');

                arrow.removeClass('hidden');
                resetLink.removeClass('active');

            } else {
                // resetLink.addClass('active');
                // resetLink2.addClass('active');
                // parentBlock.addClass('ico-hidden');
            }

        },

        events: function () {
            var inputs = $('.js-filter-drop input[type="checkbox"]');
            var resetLink = $('.filters-drop__reset');
            var resetLink2 = $('.filters-select__list-reset');

            inputs.on('change', function (e) {
                filters.writeChanges($(this));
            });
        }

    }

    return {
        init: function () {
            filters.events();
        }
    }
}())

/*price slider*/
function priceSelect() {
    if ($('.js-filter-price-btn').length) {


        $('.js-filter-price-btn').on('click', function (e) {
            var span_start = $('.js-first-amount');
            var span_end = $('.js-last-amount');
            var input_val_from = $('input#js-price-slider-input-from').val();
            var input_val_to = $('input#js-price-slider-input-to').val();

            var cur = $(this),
                holder = '.js-filter-drop-parent',
                active_text = $('.js-filter-active-text-price'),
                hide_text = $('.js-filter-placeholder'),
                btn = $('.js-filter-btn');


            span_start.html(input_val_from);
            span_end.html(input_val_to);

            cur.parents(holder).find($('.js-filter-btn-arrow')).addClass('hidden');
            cur.parents(holder).find($('.js-filter-btn-cross')).addClass('active');
            cur.parents(holder).find(btn).addClass('js-full');
            cur.parents(holder).find(active_text).addClass('active');
            cur.parents(holder).find($('.js-filter-drop')).slideUp(200);
            return false;
        })

        $('.js-filter-btn-cross').on('click', function () {

            $('.js-filter-btn').removeClass('active');
            $(this).parents('.js-filter-drop-parent').find($('input')).each(function () {
                $(this).val($(this).attr('value'));
            })
            $(this).parents('.js-filter-drop-parent').find($('.js-filter-active-text-price')).removeClass('active');


        })
    }


}
/*price slider end*/


/*sizes slider*/
/*sizes slider end*/

/*YANDEX MAPADDRESS*/
function mapAddress() {
    if ($('#js-map-address').length) {
        jQuery.getScript("http://api-maps.yandex.ru/2.0/?load=package.full&amp;lang=ru-RU", function () {
            var myMap;

            function init() {
                myMap = new ymaps.Map("js-map-address", {
                    center: [55.755814, 37.617635],
                    zoom: 12
                });
                var myPlacemark = new ymaps.Placemark([55.778699, 37.512371], {
                    balloonContent: '<div class="map-baloon"><div class="map-baloon__row"><h2 class="map-baloon__title">Полежаевская</h2></div><div class="map-baloon__row"><div class="map-baloon__subtitle">Адрес</div><div class="map-baloon__content">м. Полежаевская, ул. Зорге, д.1, стр. 2,<br/>ТЦ "Дом для Дома", 2-й этаж</div></div><div class="map-baloon__row"><div class="map-baloon__subtitle">График работы</div><div class="map-baloon__content">10:00 до 20:00 без выходных</div></div><div class="map-baloon__row"><div class="map-baloon__subtitle">Телефоны</div><div class="map-baloon__content"><span class="map-baloon__phone">+7 (495) 740-28-43</span><span class="map-baloon__phone">+7 (495) 255-18-10</span></div></div></div>'
                }, {
                    iconImageHref: 'img/map-pin-orange.svg',
                    iconImageSize: [49, 61],
                    hideIconOnBalloonOpen: false
                });


                myMap.geoObjects.add(myPlacemark);


                myPlacemark.events
                    .add('mouseenter', function (e) {
                        e.get('target').options.set({iconImageHref: 'img/map-pin.svg'});
                    })
                    .add('mouseleave', function (e) {
                        e.get('target').options.set({iconImageHref: 'img/map-pin-orange.svg'});
                    });
            }

            ymaps.ready(init);
        });
    }
}
/*YANDEX MAPADDRESS END*/

/*YANDEX MAPCARD*/
function mapCard() {
    if ($('#js-map-card').length) {
        jQuery.getScript("http://api-maps.yandex.ru/2.0/?load=package.full&amp;lang=ru-RU", function () {
            var myMap;

            function init() {
                myMap = new ymaps.Map("js-map-card", {
                    center: [55.755814, 37.617635],
                    zoom: 12
                });
                var myPlacemark = new ymaps.Placemark([55.778699, 37.512371], {
                    balloonContent: '<div class="map-baloon"><div class="map-baloon__row"><h2 class="map-baloon__title">Полежаевская</h2></div><div class="map-baloon__row"><div class="map-baloon__subtitle">Адрес</div><div class="map-baloon__content">м. Полежаевская, ул. Зорге, д.1, стр. 2,<br/>ТЦ "Дом для Дома", 2-й этаж</div></div><div class="map-baloon__row"><div class="map-baloon__subtitle">График работы</div><div class="map-baloon__content">10:00 до 20:00 без выходных</div></div><div class="map-baloon__row"><div class="map-baloon__subtitle">Телефоны</div><div class="map-baloon__content"><span class="map-baloon__phone">+7 (495) 740-28-43</span><span class="map-baloon__phone">+7 (495) 255-18-10</span></div></div></div>'
                }, {
                    iconImageHref: 'img/map-pin.svg',
                    iconImageSize: [49, 61],
                    hideIconOnBalloonOpen: false
                });

                var myPlacemark2 = new ymaps.Placemark([55.761051, 37.650697], {
                    balloonContent: '<div class="map-baloon"><div class="map-baloon__row"><h2 class="map-baloon__title">Полежаевская</h2></div><div class="map-baloon__row"><div class="map-baloon__subtitle">Адрес</div><div class="map-baloon__content">м. Полежаевская, ул. Зорге, д.1, стр. 2,<br/>ТЦ "Дом для Дома", 2-й этаж</div></div><div class="map-baloon__row"><div class="map-baloon__subtitle">График работы</div><div class="map-baloon__content">10:00 до 20:00 без выходных</div></div><div class="map-baloon__row"><div class="map-baloon__subtitle">Телефоны</div><div class="map-baloon__content"><span class="map-baloon__phone">+7 (495) 740-28-43</span><span class="map-baloon__phone">+7 (495) 255-18-10</span></div></div></div>'
                }, {
                    iconImageHref: 'img/map-pin-orange.svg',
                    iconImageSize: [49, 61],
                    hideIconOnBalloonOpen: false
                });
                myMap.geoObjects.add(myPlacemark);
                myMap.geoObjects.add(myPlacemark2);
                myPlacemark.events
                    .add('mouseenter', function (e) {
                        e.get('target').options.set({iconImageHref: 'img/map-pin-orange.svg'});
                    })
                    .add('mouseleave', function (e) {
                        e.get('target').options.set({iconImageHref: 'img/map-pin.svg'});
                    });
                myPlacemark2.events
                    .add('mouseenter', function (e) {
                        e.get('target').options.set({iconImageHref: 'img/map-pin.svg'});
                    })
                    .add('mouseleave', function (e) {
                        e.get('target').options.set({iconImageHref: 'img/map-pin-orange.svg'});
                    });

            }

            ymaps.ready(init);
        });
    }
}
/*YANDEX MAPCARD END*/

/*VIDEO LOAD*/
/* video */
function videoLoad() {
    if ($('.js-video-holder').length) {
        $('.js-video-holder').each(function () {
            var cur = $(this);
            var cur_attr = cur.attr("data-video");
            var cur_video = cur.find(".js-video-img");
            cur.on("click", function () {
                cur.addClass("active");
                cur_video.replaceWith('<iframe class="video" frameborder="0" allowfullscreen src="https://www.youtube.com/embed/' + cur_attr + '?autoplay=1&showinfo=0&vq=hd720"></iframe>')
            });
        });
    }
}
/*video end */
/*VIDEO LOAD END*/

function inputLabel() {
    $('.input-label').on('focusout', function () {
        var length = $(this).val().length;
        if (length != 0) {
            $(this).addClass('fix');
        } else {
            $(this).removeClass('fix');
        }
    });
}

/*equal hights*/
function equalHight() {
    if ($('.js-equal-height').length) {
        $('.js-equal-height .products-item').equalHeights();
    }
}
/*equal hights end*/

/*append gallery*/


/*append gallery end*/

/**/

//rebuild functions

function colorChecker() {
    if ($('.js-color-picker').length) {

        $('.js-color-picker .d-filter__color-item').each(function (i,el) {
           var self = $(el);

           if(self.hasClass('active')) {
               self.find('.d-filter__cb').prop('checked', true)
           }
        });

        $('.js-color-picker .d-filter__color-item').on('click', function () {
            var checkbox = $(this).find('.d-filter__cb');
            var thisChecked = checkbox.prop('checked');
            thisChecked = !thisChecked;

            $(this).toggleClass('active');

            checkbox.prop('checked', thisChecked);
        });
    }
}

function showTags(){
    if($('.catalog__tags').length) {
        $('.tags__more').on('click', function () {
            $(this).siblings('.tags-list').toggleClass('opened');
        })
    }
}

function showFiltrs(){
    if($('.d-filter__more').length) {
        $('.js-filter-more').on('click', function () {
            $(this).closest('.d-filter__main-item--holder').find('.checkboxes__item.hidden').toggleClass('opened');
        })
    }
}

function addFixInput() {
    if ($('.d-filter').length) {

        $('.d-filter .input').each(function () {
            if ($(this).val()) {
                $(this).addClass('fix');
            }
        })
    }
}

function rangeSlider() {
    if ($('.js-range-slider').length) {
        var from = $("#range-from");
        var to = $("#range-to");
        var range = $("#slider-range");
        var minmax = [+range.attr('data-min-range'), +range.attr('data-max-range')];

        range.slider({
            range: true,
            min: minmax[0],
            max: minmax[1],
            values: [minmax[0], minmax[1]],
            slide: function (event, ui) {
                from.val(ui.values[0]);
                to.val(ui.values[1]);
            }

        });
        from.val(range.slider("values", 0));
        to.val(range.slider("values", 1));

        from.on('change', function () {
            if ($(this).val() >= minmax[0] && $(this).val() <= minmax[1] && isNumber($(this).val())) {
                range.slider("values", 0, $(this).val());
            } else {
                console.warn('slider input out of range or the value isNaN');
            }

        });
        to.on('change', function () {
            if ($(this).val() >= minmax[0] && $(this).val() <= minmax[1] && isNumber($(this).val())) {
                range.slider("values", 1, $(this).val());
            } else {
                console.warn('slider input out of range or the value isNaN');
            }
        })

    }
}

function rangeSlider_1() {
    if ($('.d-filter__main-item--holder').length) {
        var from = $("#height1");
        var to = $("#height2");
        var range = $("#slider-range_1");
        var minmax = [+range.attr('data-min-range'), +range.attr('data-max-range')];

        range.slider({
            range: true,
            min: minmax[0],
            max: minmax[1],
            values: [minmax[0], minmax[1]],
            slide: function (event, ui) {
                from.val(ui.values[0]);
                to.val(ui.values[1]);
            }

        });
        from.val(range.slider("values", 0));
        to.val(range.slider("values", 1));

        from.on('change', function () {
            if ($(this).val() >= minmax[0] && $(this).val() <= minmax[1] && isNumber($(this).val())) {
                range.slider("values", 0, $(this).val());
            } else {
                console.warn('slider input out of range or the value isNaN');
            }

        });
        to.on('change', function () {
            if ($(this).val() >= minmax[0] && $(this).val() <= minmax[1] && isNumber($(this).val())) {
                range.slider("values", 1, $(this).val());
            } else {
                console.warn('slider input out of range or the value isNaN');
            }
        })

    }
}

function rangeSlider_2() {
    if ($('.d-filter__main-item--holder').length) {
        var from = $("#height3");
        var to = $("#height4");
        var range = $("#slider-range_2");
        var minmax = [+range.attr('data-min-range'), +range.attr('data-max-range')];

        range.slider({
            range: true,
            min: minmax[0],
            max: minmax[1],
            values: [minmax[0], minmax[1]],
            slide: function (event, ui) {
                from.val(ui.values[0]);
                to.val(ui.values[1]);
            }

        });
        from.val(range.slider("values", 0));
        to.val(range.slider("values", 1));

        from.on('change', function () {
            if ($(this).val() >= minmax[0] && $(this).val() <= minmax[1] && isNumber($(this).val())) {
                range.slider("values", 0, $(this).val());
            } else {
                console.warn('slider input out of range or the value isNaN');
            }

        });
        to.on('change', function () {
            if ($(this).val() >= minmax[0] && $(this).val() <= minmax[1] && isNumber($(this).val())) {
                range.slider("values", 1, $(this).val());
            } else {
                console.warn('slider input out of range or the value isNaN');
            }
        })

    }
}

function rangeSlider_3() {
    if ($('.d-filter__main-item--holder').length) {
        var from = $("#height5");
        var to = $("#height6");
        var range = $("#slider-range_3");
        var minmax = [+range.attr('data-min-range'), +range.attr('data-max-range')];

        range.slider({
            range: true,
            min: minmax[0],
            max: minmax[1],
            values: [minmax[0], minmax[1]],
            slide: function (event, ui) {
                from.val(ui.values[0]);
                to.val(ui.values[1]);
            }

        });
        from.val(range.slider("values", 0));
        to.val(range.slider("values", 1));

        from.on('change', function () {
            if ($(this).val() >= minmax[0] && $(this).val() <= minmax[1] && isNumber($(this).val())) {
                range.slider("values", 0, $(this).val());
            } else {
                console.warn('slider input out of range or the value isNaN');
            }

        });
        to.on('change', function () {
            if ($(this).val() >= minmax[0] && $(this).val() <= minmax[1] && isNumber($(this).val())) {
                range.slider("values", 1, $(this).val());
            } else {
                console.warn('slider input out of range or the value isNaN');
            }
        })

    }
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function isMobile() {
    return window.innerWidth < 992
}

function moreFilters() {
    if ($('.d-filter').length) {
        var filterTrigger = $('.js-filter-more');
        var closedTxt = filterTrigger.html();
        var openedTxt = filterTrigger.attr('data-opened-txt');

        filterTrigger.on('click', function () {
            var self = $(this);

            self.toggleClass('opened');

            if (self.hasClass('opened')) {
                $('.js-filter-more-drop').slideDown(200);
                self.html(openedTxt);
            } else {
                $('.js-filter-more-drop').slideUp(200);
                self.html(closedTxt);
            }
        });

        $('.d-filter__other-hdr').on('click', function () {
            //if (isMobile()) {
                $(this).next('.d-filter__other-item-holder').slideToggle(200);
                $(this).parent().toggleClass('active');
            //}
        })
		$('.d-filter__new-hdr').on('click', function () {
            //if (isMobile()) {
                $(this).next('.d-filter__main-item--holder').slideToggle(200);
                $(this).parent().toggleClass('active');
            //}
        })

    }
}

function plateSlider() {
    var slidesNumbers = $('.i-plates__item').length;

    if ($('.i-plates').length) {

        $('.js-i-plates-slider').on('init', function () {
            $('.i-plates__btn-next').show();
            $('.i-plates__btn-prev').hide();
        });

        $('.js-i-plates-slider').slick({
            slidesToScroll: 8,
            slidesToShow: 8,
            variableWidth: true,
            infinite: false,
            speed: 200,
            //draggable: true,
            //touchMove: true,
            //swipeToSlide: false,
            //swipe: false,

            nextArrow: '<div class="i-plates__btn i-plates__btn-next"><svg><use xlink:href="#arrow-tobottom"></use></svg></div>',
            prevArrow: '<div class="i-plates__btn i-plates__btn-prev"><svg><use xlink:href="#arrow-tobottom"></use></svg></div>',


            responsive: [
                {
                    breakpoint: 1300,
                    settings: {
                        slidesToScroll: 7,
                        slidesToShow: 7
                    }
                },
                {
                    breakpoint: 1140,
                    settings: {
                        slidesToScroll: 6,
                        slidesToShow: 6
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToScroll: 5,
                        slidesToShow: 5
                    }
                },
                {
                    breakpoint: 835,
                    settings: {
                        slidesToScroll: 4,
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 675,
                    settings: {
                        slidesToScroll: 3,
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 530,
                    settings: {
                        slidesToScroll: 2,
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 395,
                    settings: {
                        slidesToScroll: 1,
                        slidesToShow: 1
                    }
                }
            ]
        });


        $('.js-i-plates-slider').on('afterChange', function (event, slick, currentSlide, nextSlide) {
            var shownSlides = $('.js-i-plates-slider').slick('slickGetOption', 'slidesToShow');

            if (currentSlide === 0) {
                $('.i-plates__btn-next').show();
                $('.i-plates__btn-prev').hide();
                $('.js-i-plates-slider').css({
                    'padding-right': '48px',
                    'padding-left': 0
                });
            }
            if ((currentSlide <= slidesNumbers) && ((shownSlides + currentSlide) >= slidesNumbers)) {

                $('.i-plates__btn-next').hide();
                $('.i-plates__btn-prev').show();
                $('.js-i-plates-slider').css({
                    'padding-right': 0,
                    'padding-left': '48px'
                });
            }
        });
    }
}

function basketButton() {
    var elem = $('.js-basket-button');
    if (elem.length) {
        elem.on('click', function () {
            var self = $(this);
           var txt = self.attr('data-text-toggle');           
           var redir = self.attr('data-basket-link');

           if(self.hasClass('selected')) {
               document.location.href = redir;
           } else {
               if(txt.length > 0) {
                   self.find('.btn__text').html(txt);
               }
               self.addClass('selected');
           }           
        });
    }
}

//rebuild functions END

$(document).ready(function () {
    placeholder($('input[placeholder], textarea[placeholder]'));
    accordeon();
    sidebarAccordeon();
    catalogDrop();
    basketDrop();
    menuDrop();
    map();
    // mapAddress();
    mapCard();
    sliders();
    customSelect($('.select'), selectConfigDefault);
    scrollbarVertical($('.js-scroll'));
    spinner();
    inputLabel();
    autoFooter();
    priceSelect();
    filterDrop();
    inputMask();
    inputLabel();
    fancybox();
    tabs();

    videoLoad();
    equalHight();
    filterTablet();

    filtersProduct.init();

    scrollbarHorizontal($('.js-scroll-horizontal'));
    scrollbarVertical($('.js-scroll-vertical'));

    showTags();
	showFiltrs();
    colorChecker();
    rangeSlider();
	rangeSlider_1();
	rangeSlider_2();
	rangeSlider_3();
    addFixInput();
    moreFilters();
    plateSlider();
    basketButton();

    $(".js-mask").mask("+7(999) 9999-99-99");
});

$(document).ready(function(){
	if (document.body.clientWidth <= '1024') {
		/* filters product end */
		var main = function() { //главная функция
			var open_button = $('.drop-item__heading');
			open_button.click(function() { 
				$('.js-main-menu-list').addClass('no_overflow');
				$(this).siblings($('.drop-item__sub')).animate({ //выбираем класс menu и метод animate

					right: '0',
					width:'100%'

				}, 50); //скорость движения меню в мс
			});


		/* Закрытие меню */
			var close_button = $('.return_mob_menu');
			close_button.click(function() { //выбираем класс icon-close и метод click
				$('.js-main-menu-list').removeClass('no_overflow');
				$(this).closest($('.drop-item__sub')).animate({ //выбираем класс menu и метод animate

					//rigt: '100%',
					width:'0'

				}, 50); //скорость движения меню в мс

			});
			$('.js-main-menu-trigger').click(function() {
				$('.js-main-menu-list').removeClass('no_overflow');
				$('.drop-item__sub').animate({ //выбираем класс menu и метод animate

					//rigt: '100%',
					width:'0'

				}, 50);
			});
		};

		$(document).ready(main);
	}
});
